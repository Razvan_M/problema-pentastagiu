package ro.pentalog.Problema_3;

import java.util.*;
import java.lang.*;
import java.io.*;

class P3
{
	public static void main (String[] args) throws java.lang.Exception
	{
		int i,j;
		int sumCurent=0, sumMax=0;
		int[] init = {1, 2, 1, 5, 2, 3, 1, 0, 1, 2, 6, 4, 5, 2, 3, 4, 1, 2};
        System.out.print("Vector initial: ");
        for (i=0; i<init.length; i++)
       		System.out.print(init[i]+" ");
       	System.out.println("\n");
       	i=0;
       		for (j =1;j<init.length-1; j++)
       		{
       			if (init[i] > init[j])
       				sumCurent+=init[i]-init[j];
       			else
   				{
   					if(sumCurent>sumMax)
	   					{
	   						sumMax=sumCurent;
	   					}
   					sumCurent=0;
   					i=j;
   				}
       		}
			sumCurent=0;i=init.length-1;
       		for (j = init.length-2; j>0 ; j--)
       		{
       			if (init[j] < init[i])
       				sumCurent+=init[i]-init[j];
   				else
   					{
   						if (sumCurent>sumMax)
	   						{
	   							sumMax=sumCurent;
	   						}
   						sumCurent=0;
   						i=j;
   					}
       		}
       	
       	System.out.println("The biggest block of water contains "+sumMax+" units of water.");
	}
}