3. Given a sequence of N numbers, imagine each number represents the height for a specific block. 
Find the volume of the biggest block of water that would hypothetically occur after heavy rain or pouring water on the structure.

Eg: The following sequence will generate the structure below(consider the structure is sided by higher transparent walls):
Sequence: 1 2 1 5 2 3 1 0 1 2 6 4 5 2 3 4 1 2
	
The biggest block of water would contain 21 units of water.